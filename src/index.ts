import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import express from 'express';
import rateLimit from 'express-rate-limit';
import compression from 'compression';
import Routes from './routes';

import { logger } from './utilities/logging';
import { LiveConfig } from './utilities/live-config';
import { BaseCtrl } from './controllers/base.ctrl';

const app = express();
const port = process.env.NODE_PORT || 3000;
const env = (process.env.NODE_ENV || 'local').toLowerCase();
const limiter = rateLimit({ windowMs: 15 * 60 * 1000, max: 250 });
const baseCtrl = new BaseCtrl('src-index');

app.use(cors({ exposedHeaders: ['Content-Type'] }));
app.use(compression());
app.use(helmet());
app.use(limiter);
app.use(bodyParser.json({ limit: '1mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('trust proxy', true);
app.set('ENV', env);

LiveConfig.setApp(app);
baseCtrl.getAllConfigs()
  .then(() => {
    Routes(app);

    app.listen(port, () => {
      logger.info(`Listening on port: ${port}`);
    });
  })
  .catch((e) => {
    logger.error(e);
    process.exit(1);
  });
