import { firestore } from 'firebase-admin';

import admin from '../utilities/firebase-service';
import { logger } from '../utilities/logging';
import { LiveConfig } from '../utilities/live-config';
import { SharedConfig, SystemVars } from '../interfaces/system';
import _ from 'lodash';
import allConfigs from './configs.json';
import CollectionReference = firestore.CollectionReference;

export class BaseCtrl {

  private namespace = 'BaseCtrl';

  constructor(namespace?: string) {
    if (namespace) {
      this.namespace = namespace;
    }
  }

  dbRef(collection: string): CollectionReference {
    return admin.firestore().collection(collection);
  }

  async getAllConfigs(): Promise<void> {
    try {
      if (process.env.NODE_ENV === 'local') {
        Object.keys(allConfigs).map((k) => LiveConfig.setVariable(k, allConfigs[k]));
        logger.info('Serving local config');
        return;
      }

      return await this.dbRef('shared_config')
        .get()
        .then((snapshot) => {
          if (snapshot.empty) {
            logger.error('Something is not right');
            return;
          }

          snapshot.docs.map((doc) => {
            LiveConfig.setVariable(_.camelCase(doc.id), doc.data());
          });
          return;
        });
    } catch (e) {
      this.handleError(e);
    }
  }

  async getSharedConfig(docId: SystemVars, overwrite = false): Promise<void> {
    try {
      if (!overwrite) {
        return;
      }

      return await this.dbRef('shared_config')
        .doc(docId)
        .get()
        .then((doc) => {
          if (!doc.exists) {
            logger.error('Something is not right');
            return;
          }

          LiveConfig.setVariable(_.camelCase(docId), doc.data());
          return;
        });
    } catch (e) {
      this.handleError(e);
    }
  }

  get config(): SharedConfig {
    return LiveConfig.getAll();
  }

  handleError(e: Error): void {
    logger.error({ error: e, stack: e.stack });
    throw { message: e.message, error: e, stack: e.stack };
  }

}
