import https from 'https';
import _ from 'lodash';
import {
  DataEntity,
  IhPermissions,
  LocationsEntity,
  MetaResponse,
  MtEmployeeObject,
  MtEmployees,
  MtEmployeesFB,
  MtRegions,
  MtRequestOptions,
  MtTenant,
  Self
} from '../interfaces/mt';
import { BaseCtrl } from './base.ctrl';


export class MtApiCtrl extends BaseCtrl {

  private readonly mtAdminToken: string = process.env.MT_ADMIN_TOKEN;
  public readonly ihPermissions: IhPermissions[] = this.config.insightsApp.userPermissions;
  public readonly ihAdminPermissions: IhPermissions[] = this.config.insightsApp.adminPermissions;

  constructor() {
    super('MtApi');
  }

  async apiCall(token: string, endpoint: string, domain: string, options?: MtRequestOptions): Promise<any> {
    options = _.defaultsDeep(options, { page_size: 100, page: 1 });
    const queryString = this.jsonToQuerystring(options);
    endpoint = [endpoint, queryString].join('?');
    const headers = {
      hostname: `${domain}.marianatek.com`,
      protocol: 'https:',
      path: endpoint,
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return new Promise((resolve, reject) => {
      const request = https.request(headers, (response) => {
        const responseBody = [];

        response.on('data', (chunk) => {
          responseBody.push(chunk);
        });

        response.on('end', () => {
          try {
            const data = JSON.parse(responseBody.join(''));
            if (_.has(data, 'errors')) {
              // indicates an invalid request
              reject(data);
            } else {
              resolve(data);
            }
          } catch (e) {
            // also indicates invalid request
            reject(responseBody);
          }
        });
      });

      request.on('error', (e) => {
        reject(e);
      });

      request.end();
    });
  }

  async checkDomain(domain: string): Promise<MtTenant> {
    try {
      return await this.dbRef('brands')
        .where('subDomain', '==', domain)
        .get()
        .then((docs) => {
          if (docs.empty) {
            throw { errors: { detail: 'schema not found' } };
          }

          return docs.docs.pop().data();
        }) as MtTenant;
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

  async getSelf(token: string, domain: string): Promise<Self> {
    try {
      const userAccount = await this.apiCall(token, '/api/users/self', domain) as Self;

      return this.parseUser(userAccount, _.get(userAccount, 'data.id'), domain);
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

  async getEmployeeObject(domain: string, user_email: string): Promise<MtEmployeeObject> {
    try {
      const employee = await this.apiCall(this.mtAdminToken, '/api/employees', domain, { user_email });
      const employeeData = _.get(employee, 'data[0]');
      const userId = _.get(employeeData, 'relationships.user.data.id');
      const mappedLocations = await this.getLocations(domain, userId);
      const userDetails = await this.apiCall(this.mtAdminToken, `/api/users/${userId}`, domain) as Self;
      const parsedUserFields = this.parseUser(userDetails, userId, domain);
      const tenant = await this.checkDomain(domain);

      return {
        self: parsedUserFields,
        locations: mappedLocations,
        tenants: [{
          tenantId: tenant.tenantId,
          name: tenant.displayName,
          schemaName: tenant.schemaName,
          brandId: tenant.brandId,
        }],
      };
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

  async getEmployees(domain, page = 1, page_size = 100): Promise<MtEmployees> {
    try {
      const employees = await this.apiCall(this.mtAdminToken, '/api/users', domain, {
        page_size,
        page,
        is_employee: true,
      }) as MtEmployees;

      const employeesData = _.get(employees, 'data');
      employees.data = employeesData.map((employee) => _.pick(employee.attributes, ['full_name', 'email'])) as unknown as DataEntity[];

      return employees;
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

  async getEmployeesFb(domain: string, page: number = 1, perPage: number = 500): Promise<MtEmployeesFB> {
    try {
      const res = {};
      const fullDomain = await this.checkDomain(domain);

      if (!fullDomain) {
        return Promise.reject({ error: { details: 'domain not found', domain } });
      }

      const { schemaName } = fullDomain;

      if (!schemaName) {
        return Promise.reject({ error: { details: 'schemaName not found', fullDomain } });
      }

      const snapshot = await this.dbRef('brands')
        .doc(schemaName)
        .collection('employees')
        .get();
      const employeeList = snapshot.docs.map(doc => doc.data());
      res['meta'] = {
        pagination: {
          count: employeeList.length,
          page,
          pages: Math.round(employeeList.length / perPage),
          per_page: perPage,
        },
      };
      res['data'] = this.paginate(employeeList, perPage, page);

      return res as MtEmployeesFB;
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

  async getDomainLocations(domain, page = 1, page_size = 100): Promise<MetaResponse> {
    try {
      const locations = await this.apiCall(this.mtAdminToken, `/api/locations`, domain, { page_size, page }) as any;
      const locationsParsed: MetaResponse['data'] = locations.data.map((l) => {
        return { id: l.id, name: l.attributes.name };
      });

      return { meta: locations.meta, data: locationsParsed };
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

  async getDomainPartners(domain, page = 1, page_size = 100): Promise<MetaResponse> {
    try {
      const partners = await this.apiCall(this.mtAdminToken, `/api/partners`, domain, { page_size, page }) as any;
      const partnersParsed: MetaResponse['data'] = partners.data.map((p) => {
        return { id: p.id, name: p.attributes.name };
      });

      return { meta: partners.meta, data: partnersParsed };
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

  async getLocations(domain: string, userId: string): Promise<LocationsEntity[]> {
    try {
      const employee = await this.apiCall(this.mtAdminToken, '/api/employees', domain, { user: userId });
      const employeeData = _.pick(employee, 'data') as any;
      const partners = await this.apiCall(this.mtAdminToken, '/api/partners', domain, {
        partner_type: 'location',
        page_size: 100,
      });
      const partnerData = _.get(partners, 'data');
      let partnerIdMap = {};
      partnerData.map((partner) => {
        const partnerDataId = _.get(partner, 'relationships.location.data.id');
        if (partnerDataId && partner.id) {
          partnerIdMap[partnerDataId] = partner.id;
        }
      });
      const regions = await this.getRegions(domain, employeeData.data[0].id);
      const mappedLocations = [];

      for (let i = 0; i < regions.length; i++) {
        let locations = _.get(regions[i], 'locations', []);

        for (let j = 0; j < locations.length; j++) {
          if (locations[j].enabled) {
            let { name, id } = locations[j];
            let partnerId = partnerIdMap[id];
            mappedLocations.push({ name, id: partnerId });
          }
        }
      }

      return mappedLocations;
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

  async getRegions(domain: string, employeeId: string): Promise<MtRegions[]> {
    const employeeDetails = await this.apiCall(this.mtAdminToken, `/api/employees/${employeeId}`, domain, { include: 'turf.locations' });
    return _.get(employeeDetails, 'data.attributes.turf.regions');
  }

  paginate(array, perPage, page): number[] {
    return array.slice((page - 1) * perPage, page * perPage);
  }

  parseUser(userDetails: Self, userId: string, domain: string): Self {
    const userFields = ['email', 'first_name', 'last_name', 'full_name', 'birth_date', 'company_name', 'permissions'];
    const userData = _.get(userDetails, 'data.attributes');
    const pickedFields = _.pick(userData, userFields) as unknown as Self;
    pickedFields.userId = userId;
    pickedFields.permissions = _.intersection(this.ihPermissions, (pickedFields.permissions || []));
    pickedFields.domain = domain;

    return pickedFields;
  }

  jsonToQuerystring(json): string {
    return Object
      .keys(json)
      .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(json[key])}`)
      .join('&');
  }

}
