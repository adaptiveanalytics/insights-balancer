import { BaseCtrl } from './base.ctrl';
import { SystemVars } from '../interfaces/system';

export class WebhooksCtrl extends BaseCtrl {
  constructor() {
    super('Webhooks');
  }

  async updateLocalVariable(key: SystemVars): Promise<void> {
    try {

      switch (key) {
        case 'insights_app':
        case 'insights_balancer':
          await this.getSharedConfig(key, true);
          return;
      }

    } catch (e) {
      this.handleError(e);
    }
  }

}
