import querystring from 'node:querystring';
import * as _ from 'lodash';
import { DashboardRequest, ReportRequest } from '../interfaces/requests';
import { Dashboard, Report } from '../interfaces/dashboard';
import { BaseCtrl } from './base.ctrl';
import { normalizeUrl } from '../utilities/helpers';


export class RequestsCtrl extends BaseCtrl {

  private readonly dataServerUrl: string = this.config.insightsDataServer.baseUrl;

  constructor() {
    super('Requests');
  }

  async dataServerRequest(report: Report): Promise<any> {
    try {
      // return report;
      return _.pick(report, ['dataServerUrl', 'dataServerQuery', 'requestPayload', 'requestMethod', 'id']);
    } catch (e) {
      this.handleError(e);
    }
  }

  async getDashboardById(requestObject: DashboardRequest): Promise<Dashboard | void> {
    try {
      return await this.dbRef('dashboards')
        .doc(requestObject.id)
        .get()
        .then(async (doc) => {
          if (!doc.exists) {
            throw { message: `${requestObject.id} was not found in the dashboards collection` };
          }

          const dashboard = doc.data() as Dashboard;
          dashboard.id = doc.id;
          const reports: Report[] = [];

          for (let i = 0; i < dashboard.reportIds.length; i++) {
            const id = dashboard.reportIds[i];
            const reportRequest: ReportRequest = _.defaultsDeep({ id }, requestObject);
            const report = await this.getReportById(reportRequest) as Report;
            reports.push(report);
          }

          dashboard.reports = reports;

          return dashboard;
        })
        .catch(this.handleError);
    } catch (e) {
      this.handleError(e);
    }
  }

  async getReportById(reportRequest: ReportRequest): Promise<Report | void> {
    try {
      return await this.dbRef('reports').doc(reportRequest.id)
        .get()
        .then(async (doc) => {
          if (!doc.exists) {
            console.error('We have a problem');
            throw { message: `${reportRequest.id} was not found in the dashboards collection` };
          }

          const report = doc.data() as Report;
          report.id = doc.id;
          const normalizedReport = await this.normalizeRequest(reportRequest, report);

          return await this.dataServerRequest(normalizedReport);
        })
        .catch(this.handleError);
    } catch (e) {
      this.handleError(e);
    }
  }

  async normalizeRequest(reportRequest: ReportRequest, report: Report): Promise<Report> {
    try {
      report.dataServerUrl = normalizeUrl([this.dataServerUrl, report.dataServerUrl]);

      const payload: { [key: string]: any } = _.pick(report, ['id', 'storedProcedure']);
      report.parameters?.map((p) => {
        const val = _.get(reportRequest.parameters, p);

        if (val) {
          payload[p] = val;
        }
      });

      switch (report.requestMethod) {
        case 'get':
          report.dataServerQuery = querystring.encode(payload);
          break;
        default:
          report.requestPayload = payload;
      }

      return report;
    } catch (e) {
      this.handleError(e);
    }
  }

}
