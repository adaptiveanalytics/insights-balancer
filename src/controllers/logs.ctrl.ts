import path from 'path';
import fs from 'fs-extra';
import { LogConfig } from '../config/log.config';

export class LogsCtrl {

  async fileDetails(filename: string): Promise<any> {
    try {
      return fs.readFileSync(path.join(LogConfig.dir, filename), 'utf8');
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

  async listFiles(level: string | undefined): Promise<string[] | any> {
    try {
      return fs.readdirSync(LogConfig.dir).filter((f) => {
        if (level) {
          return f.includes('.log') && f.includes(level);
        }

        return f.includes('.log');
      });
    } catch (e) {
      throw { message: e.message, error: e, stack: e.stack };
    }
  }

}
