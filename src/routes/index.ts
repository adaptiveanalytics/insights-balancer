import { Router } from 'express';

// Import middleware
import Middleware from '../middleware';
import ErrorHandler from '../middleware/error-handler';

// Import our routes
import system from './unversioned';
import v1 from './v1';

export default (app) => {

  // @ts-ignore
  const routes = new Router({});

  // Un-versioned Routes
  routes.use('/system', system());

  // Versioned Routes
  routes.use('/v1', v1());

  // Health Check Route
  routes.get('/health-check', (req, res) => {
    const stats = {
      url: req.originalUrl,
      healthy: true,
      user: req.user,
      response_time: (Number(new Date()) - req._requestStarted) + 'ms',
    };

    res.json(stats);
  });

  Middleware(app);
  app.use('/', routes);
  ErrorHandler(app);
}
