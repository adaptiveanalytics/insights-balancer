import { Router } from 'express';

import SystemLogs from './system-logs';
import SystemTools from './system-tools';

export default () => {

  //@ts-ignore
  const routes = new Router({});

  // @todo: Add authentication
  routes.use('/logs', SystemLogs());
  routes.use('/tools', SystemTools());

  return routes;

}
