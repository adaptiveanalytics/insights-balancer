import { Router } from 'express';

export default () => {

  // @ts-ignore
  const routes = new Router({});

  // Health Check Route
  routes.get('/health-check', (req, res) => {
    const stats = {
      url: req.originalUrl,
      healthy: true,
      user: req.user,
      response_time: (Number(new Date()) - req._requestStarted) + 'ms',
    };

    res.json(stats);
  });

  routes.get('/test/:method?', async (req, res, next) => {
    try {
      const { method } = req.params;

      res.send({ method });
    } catch (e) {
      next(e);
    }
  });

  return routes;
}
