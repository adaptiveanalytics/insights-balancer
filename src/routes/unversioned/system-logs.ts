import { Router } from 'express';
import { LogsCtrl } from '../../controllers/logs.ctrl';

export default () => {

  // @ts-ignore
  const routes = new Router({});
  const logCtrl = new LogsCtrl();

  routes.get('/health-check', (req, res) => {
    const stats = {
      url: req.originalUrl,
      healthy: true,
      user: req.user,
      response_time: (Number(new Date()) - req._requestStarted) + 'ms',
      debugMode: req.debugMode,
    };

    res.json(stats);
  });

  routes.get('/list/:level?', async (req, res, next) => {
    try {
      const { level } = req.params;
      const files = await logCtrl.listFiles(level);

      res.send(files);
    } catch (e) {
      next(e);
    }
  });

  routes.get('/details/:filename', async (req, res, next) => {
    try {
      const { filename } = req.params;
      const fileContents = await logCtrl.fileDetails(filename);

      res.send(fileContents);
    } catch (e) {
      next(e);
    }
  });

  return routes;
}
