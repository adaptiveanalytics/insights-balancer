import * as Joi from 'joi';

const requestParameters = Joi.object().keys({
  date_start: Joi.string().required(),
  date_end: Joi.string().required(),
  locationIds: Joi.array().items(Joi.string()),
});

const requestBase = Joi.object().keys({
  id: Joi.string().required(),
  parameters: requestParameters,
});

export const RequestsValidation = {

  requestParameters,

  dashboard: {
    body: requestBase
  },

  report: {
    body: requestBase
  }

};
