import * as Joi from 'joi';

export const MtValidation = {

  userLocations: {
    params: Joi.object({
      domain: Joi.string().required(),
      userId: Joi.string().required().min(5),
    })
  },

  userObject: {
    params: Joi.object({
      domain: Joi.string().required(),
      email: Joi.string().email({ minDomainSegments: 2 }).required(),
    })
  }

};
