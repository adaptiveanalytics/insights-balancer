import { Router } from 'express';

import Mt from './mt';
import Requests from './requests';
import Webhooks from './webhooks';

export default () => {

  // @todo: Bill to figure out why ts-ignore needed
  // @ts-ignore
  const routes = new Router({});

  routes.get('/health-check', (req, res) => {
    const stats = {
      url: req.originalUrl,
      healthy: true,
      user: req.user,
      response_time: (Number(new Date()) - req._requestStarted) + 'ms',
    };

    res.json(stats);
  });

  routes.use('/mt', Mt());
  routes.use('/requests', Requests());
  routes.use('/hooks', Webhooks());

  return routes;
}
