import { Router } from 'express';
import { validate } from 'express-validation';
import * as _ from 'lodash';

import { MtValidation } from './validation/mt.validation';
import { checkAdmin, checkMtToken } from '../../middleware/auth';
import { MtApiCtrl } from '../../controllers/mt-api.ctrl';

export default () => {

  //@ts-ignore
  const routes = new Router({});
  const mtApi = new MtApiCtrl();

  routes.get('/health-check', (req, res) => {
    const stats = {
      url: req.originalUrl,
      healthy: true,
      user: req.user,
      response_time: (Number(new Date()) - req._requestStarted) + 'ms',
      debugMode: req.debugMode,
    };

    res.json(stats);
  });

  routes.use('/', checkMtToken);

  routes.get('/locations', async (req, res, next) => {
    try {
      const { domain, userId } = req.user;

      const locations = await mtApi.getLocations(domain, userId);

      res.send(locations);
    } catch (e) {
      next(e);
    }
  });

  routes.get('/self', async (req, res, next) => {
    try {
      if (req.debugMode) {
        return res.send(req.user);
      }

      const user = _.omit(req.user, ['uuid', 'useragent', 'token']);

      res.send(user);
    } catch (e) {
      next(e);
    }
  });

  routes.use('/', checkAdmin);

  routes.get('/user/object/:domain/:email', validate(MtValidation.userObject), async (req, res, next) => {
    try {
      const { domain, email } = req.params;
      const employee = await mtApi.getEmployeeObject(domain, email);

      res.send(employee);
    } catch (e) {
      next(e);
    }
  });

  routes.get('/user/locations/:domain/:userId', validate(MtValidation.userLocations), async (req, res, next) => {
    try {
      const { domain, userId } = req.params;
      const locations = await mtApi.getLocations(domain, userId);

      res.send(locations);
    } catch (e) {
      next(e);
    }
  });

  routes.get('/domain/partners/:domain', async (req, res, next) => {
    try {
      const { domain } = req.params;
      const { page, page_size } = req.query;
      const partners = await mtApi.getDomainPartners(domain, page, page_size);

      res.send(partners);
    } catch (e) {
      next(e);
    }
  });

  routes.get('/domain/locations/:domain', async (req, res, next) => {
    try {
      const { domain } = req.params;
      const { page, page_size } = req.query;
      const locations = await mtApi.getDomainLocations(domain, page, page_size);

      res.send(locations);
    } catch (e) {
      next(e);
    }
  });

  routes.get('/domain/employees/fb/:domain', async (req, res, next) => {
    try {
      const { domain } = req.params;
      const { page, page_size } = req.query;
      const employees = await mtApi.getEmployeesFb(domain, page, page_size);

      res.send(employees);
    } catch (e) {
      next(e);
    }
  });

  routes.get('/domain/employees/:domain', async (req, res, next) => {
    try {
      const { domain } = req.params;
      const { page, page_size } = req.query;
      const employees = await mtApi.getEmployees(domain, page, page_size);

      res.send(employees);
    } catch (e) {
      next(e);
    }
  });

  return routes;
}
