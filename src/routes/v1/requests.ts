import { Router } from 'express';
import { validate } from 'express-validation';

import { RequestsCtrl } from '../../controllers/requests.ctrl';
import { RequestsValidation } from './validation/requests.validation';
import { checkAdmin, checkMtToken } from '../../middleware/auth';
import { DashboardRequest, ReportRequest } from '../../interfaces/requests';

export default () => {

  //@ts-ignore
  const routes = new Router({});
  const reqCtrl = new RequestsCtrl();

  routes.get('/health-check', (req, res) => {
    const stats = {
      url: req.originalUrl,
      healthy: true,
      user: req.user,
      response_time: (Number(new Date()) - req._requestStarted) + 'ms',
      debugMode: req.debugMode,
    };

    res.json(stats);
  });

  routes.use('/', checkMtToken);

  routes.post('/dashboard', validate(RequestsValidation.dashboard), async (req, res, next) => {
    try {
      const request = await reqCtrl.getDashboardById(req.body as DashboardRequest);

      res.send(request);
    } catch (e) {
      next(e);
    }
  });

  routes.post('/report', validate(RequestsValidation.report), async (req, res, next) => {
    try {
      const request = await reqCtrl.getReportById(req.body as ReportRequest);

      res.send(request);
    } catch (e) {
      next(e);
    }
  });

  routes.use('/', checkAdmin);

  return routes;
}
