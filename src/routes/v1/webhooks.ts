import { Router } from 'express';
import { LiveConfig } from '../../utilities/live-config';
import { WebhooksCtrl } from '../../controllers/webhooks.ctrl';

export default () => {

  //@ts-ignore
  const routes = new Router({});
  const webhookCtrl = new WebhooksCtrl();

  routes.get('/health-check', (req, res) => {
    const stats = {
      url: req.originalUrl,
      healthy: true,
      user: req.user,
      response_time: (Number(new Date()) - req._requestStarted) + 'ms',
      debugMode: req.debugMode,
    };

    res.json(stats);
  });

  routes.get('/machine-variable/:key', async (req, res, next) => {
    try {
      const { key } = req.params;

      if (key === 'all') {
        return res.send(LiveConfig.getAll());
      }

      res.send(LiveConfig.getVariable(key));
    } catch (e) {
      next(e);
    }
  });

  routes.put('/machine-variable/:key', async (req, res, next) => {
    try {
      const { key } = req.params;
      await webhookCtrl.updateLocalVariable(key);

      if (req.debugMode) {
        return res.json(LiveConfig.getVariable(key));
      }

      res.json({ status: 'success' });
    } catch (e) {
      next(e);
    }
  });

  return routes;

}
