import _ from 'lodash';
import morgan from 'morgan';
import { ENV } from '../config';
import { routeLogger } from '../utilities/logging';

export default (app) => {

  const reqProps = ['method', 'status', 'url', 'response_time'];
  const userProps = [];

  if (ENV !== 'local') {
    reqProps.push('remote_addr');
    userProps.push('uuid');
  }

  // @ts-ignore
  app.use(morgan(reformatLog({
    http_version: ':http-version',
    method: ':method',
    referrer: ':referrer',
    remote_addr: ':remote-addr',
    remote_user: ':remote-user',
    response_time: ':response-time',
    status: ':status',
    url: ':url',
    user_agent: ':user-agent',
  })));

  function reformatLog(obj) {
    const keys = Object.keys(obj);
    const token = /^:([-\w]{2,})(?:\[([^\]]+)])?$/;

    return (tokens, req, res) => {
      const data = _.pick(req, ['user']);

      for (const i in keys) {
        const key = keys[i];
        const val = token.exec(obj[key]);
        data[key] = !_.isEmpty(val) ? tokens[val[1]](req, res, val[2]) : obj[key];
      }

      const consoleMap = _.merge(_.pick(data.user, userProps), _.pick(data, reqProps));
      const consoleArr = [];

      _.forEach(consoleMap, (val, key) => {
        consoleArr.push(`${key}: ${val}`);
      });

      routeLogger.log({ level: 'info', message: consoleArr.join(' - ') });
    };
  }

};
