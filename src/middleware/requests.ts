import { v4 } from 'uuid';
import expressUseragent from 'express-useragent';
import * as _ from 'lodash';

const UUID = 'uuid';

export default (app) => {

  app.use(expressUseragent.express());
  app.use(async (req, res, next) => {
    const uuid = req[UUID] = _.get(req.headers, UUID, v4());
    const useragent = _.pickBy(req.useragent, _.identity);

    req.user = { [UUID]: uuid, useragent };
    req._requestStarted = new Date();
    res.set(UUID, uuid);
    req.user.debugMode = req.debugMode = _.get(req.query, 'debug', false) === 'true';

    next();
  });

}
