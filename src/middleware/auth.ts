import _ from 'lodash';

import { getAuthToken } from '../utilities/auth';
import { MtApiCtrl } from '../controllers/mt-api.ctrl';
import { logger } from '../utilities/logging';
import { IhPermissions } from '../interfaces/mt';

export const checkMtToken = (req, res, next) => {
  if ((process.env.NODE_ENV || 'local').toLowerCase() === 'local' && _.get(req, 'headers.localbypass') === 'true') {
    return next();
  }

  const mtApi = new MtApiCtrl();

  getAuthToken(req, res, async () => {
    try {
      const { authToken } = req;

      if (!authToken) {
        return res.status(401).send({ error: 'Invalid credentials provided' });
      }

      const domain = _.get(req, 'headers.domain');
      const userInfo = await mtApi.getSelf(authToken, domain);

      if (userInfo) {
        userInfo['token'] = authToken;
        userInfo['domain'] = domain;
        req.user = _.merge(req.user, userInfo);
        return next();
      }

      return new Error('unauthorized');
    } catch (e) {
      logger.error(e);
      return res.status(401).send({ error: 'Invalid credentials provided' });
    }
  });
};

export const checkAdmin = (req, res, next) => {
  if ((process.env.NODE_ENV || 'local').toLowerCase() === 'local' && _.get(req, 'headers.localbypass') === 'true') {
    return next();
  }

  const mtApi = new MtApiCtrl();

  const domain = _.get(req, 'user.domain');
  const permissions = _.get(req, 'user.permissions') as IhPermissions[];
  const hasPermission = _.intersection(permissions, mtApi.ihAdminPermissions).length;

  if (['adaptiveanalytics', 'adaptiveanalytics.sandbox'].includes(domain) || hasPermission) {
    return next();
  }

  return res.status(401).send({ error: 'You are not an admin' });
};
