import Requests from './requests';
import RouteLogging from './route-logging';

export default (app) => {

  Requests(app);
  RouteLogging(app);

}
