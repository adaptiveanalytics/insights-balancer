import { ValidationError } from 'express-validation';
import _ from 'lodash';
import { logger } from '../utilities/logging';

const env = (process.env.APP_ENV || process.env.NODE_ENV || 'development').toLowerCase();

export default (app) => {

  app.use((err, req, res, _next) => {
    if (err instanceof ValidationError) {
      return res.status(err.statusCode).json(err);
    }

    if (err.message === 'validation error') {
      res.status(err.status).send({ message: err.message, errors: err.errors });
      return;
    }

    if (_.has(err, 'errors')) {
      res.status(500).send(err);
      return;
    }

    logger.error(err);

    const message = err.message || 'An error occurred';
    const payload: any = { message };

    if (['local', 'dev', 'development'].includes(env) || Object.keys(req.headers).includes('debug')) {
      payload.err = err;
    }

    res.status(500).send(payload);
  });

};
