export type IhPermissions = 'can_view_intellihub_studio_reporting' |
  'can_view_intellihub_corporate_reporting' |
  'can_view_intellihub_admin' |
  'can_view_intellihub_super_admin' |
  'ih_admin' |
  'can_intercom_chat';

export interface MetaResponse {
  meta: {};
  data: ({
    id?: string;
    name?: string;
    [key: string]: any;
  })[];
}

export interface MtRequestOptions {
  page_size?: number;
  page?: number;
  is_employee?: boolean;
  user_email?: string;
  partner_type?: string;
  user?: string;
  include?: string;
}

export interface MtAccount {
  id: string;
  account_balance?: (null)[] | null;
  address_line_one?: null;
  address_line_two?: null;
  address_line_three?: null;
  address_sorting_code?: null;
  birth_date?: null;
  city?: null;
  company_name: string;
  completed_class_count?: number;
  country: string;
  credit_cards?: (null)[] | null;
  email: string;
  emergency_contact_email?: null;
  emergency_contact_name?: null;
  emergency_contact_phone?: null;
  emergency_contact_relationship?: null;
  first_name: string;
  full_name: string;
  gender?: null;
  home_location: HomeLocation;
  is_balance_used_for_fees: boolean;
  is_marketing_allowed: boolean;
  is_opted_in_to_sms: boolean;
  is_waiver_signed: boolean;
  join_datetime: string;
  last_name: string;
  phone_number: string;
  postal_code?: null;
  pronouns?: null;
  profile_image?: null;
  state_province?: null;
  formatted_address?: (string)[] | null;
  required_legal_documents?: (null)[] | null;
  token: string;
  domain: string;
}

export interface MtEmployees {
  meta: Meta;
  links?: null;
  data?: (DataEntity)[] | null;
  included?: (null)[] | null;
}

export interface MtTenant {
  displayName: string;
  subDomain: string;
  isActive: boolean;
  brandId: number;
  schemaName: string;
  tenantId: number;
  domain: string;
}

export interface MtEmployeeObject {
  self: Self | MtAccount;
  locations?: (LocationsEntity)[] | null;
  tenants?: (TenantsEntity)[] | null;
}

export interface MtRegions {
  name: string;
  id: number;
  enabled: boolean;
  can_assign: boolean;
  locations?: (LocationsEntity | null)[] | null;
}

export interface LocationsEntity {
  name: string;
  id: string;
  currency_code?: string;
  enabled?: boolean;
  can_assign?: boolean;
}

export interface Self {
  email: string;
  first_name: string;
  last_name: string;
  full_name: string;
  birth_date?: null;
  company_name: string;
  permissions?: IhPermissions[] | null;
  userId: string;
  domain?: string;
}

export interface TenantsEntity {
  tenantId: number;
  name: string;
  schemaName: string;
  brandId: number;
}

export interface HomeLocation {
  id: string;
  address_line_one: string;
  address_line_two: string;
  address_line_three: string;
  address_sorting_code?: null;
  city: string;
  currency_code: string;
  description?: null;
  email: string;
  geo_check_in_distance: number;
  is_newsletter_subscription_pre_checked: boolean;
  latitude: string;
  longitude: string;
  name: string;
  payment_gateway_type: string;
  phone_number: string;
  postal_code: string;
  state_province: string;
  timezone: string;
  formatted_address?: (string)[] | null;
  region: Region;
}

export interface Region {
  id: string;
  name: string;
}

export interface Meta {
  pagination: Pagination;
}

export interface Pagination {
  count: number;
  pages: number;
  page: number;
  per_page: number;
}

export interface DataEntity {
  type: string;
  id: string;
  attributes: Attributes;
  relationships: Relationships;
}

export interface Attributes {
  email: string;
  first_name: string;
  last_name: string;
  birth_date?: null;
  phone_number?: string | null;
  address_line1?: string | null;
  address_line2?: string | null;
  address_line3?: null;
  city?: string | null;
  state_province?: string | null;
  postal_code?: string | null;
  address_sorting_code?: null;
  country?: string | null;
  full_name: string;
  gender?: null;
  emergency_contact_name?: null;
  emergency_contact_relationship?: null;
  emergency_contact_phone?: null;
  emergency_contact_email?: null;
  signed_waiver: boolean;
  waiver_signed_datetime?: string | null;
  date_joined: string;
  marketing_opt_in: boolean;
  is_opted_in_to_sms: boolean;
  has_vip_tag_cache: boolean;
  apply_account_balance_to_fees: boolean;
  is_minimal: boolean;
  permissions?: (string | null)[] | null;
  account_balance: number;
  account_balances?: (null)[] | null;
  third_party_sync: boolean;
  completed_class_count: number;
  company_name?: string | null;
  archived_at?: null;
  merged_into_id?: null;
  waivers?: (WaiversEntity)[] | null;
  has_unsigned_waivers: boolean;
  formatted_address?: (string | null)[] | null;
  required_legal_documents?: (null)[] | null;
  pronouns?: null;
}

export interface WaiversEntity {
  waiver_id: string;
  name: string;
  is_required: boolean;
  is_signed: boolean;
  signed_datetime?: string | null;
  broker_id?: null;
  source?: string | null;
}

export interface Relationships {
  last_region: LastRegionOrProfileImage;
  home_location: HomeLocation;
  tags: Tags;
  profile_image: LastRegionOrProfileImage;
}

export interface LastRegionOrProfileImage {
  data?: null;
}

export interface HomeLocation {
  data: DataOrDataEntity;
}

export interface DataOrDataEntity {
  type: string;
  id: string;
}

export interface Tags {
  data?: (DataOrDataEntity1 | null)[] | null;
}

export interface DataOrDataEntity1 {
  type: string;
  id: string;
}

export interface MtEmployeesFB {
  meta: Meta;
  data?: (FbDataEntity)[] | null;
}

export interface FbDataEntity {
  isActive: boolean;
  email: string;
  userId: number;
  fullName: string;
}

