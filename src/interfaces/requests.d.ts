import { LocationsEntity } from './mt';

interface RequestParametersBase {
  date_end?: string;
  date_start?: string;
}

export interface RequestParameters extends RequestParametersBase {
  [key: string]: any;
}

export interface Requests {
  id: string;
  parameters: RequestParameters;
}

export interface DashboardRequest extends Requests {
  parameters: {
    locationIds?: LocationsEntity['id'][];
  };
}

export interface ReportRequest extends Requests {
  parameters: {
    locationIds?: LocationsEntity['id'][];
  };
}
