import { IhPermissions } from './mt';

export type SystemVars = 'insights_balancer' | 'insights_app' | 'insights_data_server';

export interface InsightsApp {
  [key: string]: any;
  userPermissions: IhPermissions[];
  adminPermissions: IhPermissions[];
}

export interface InsightsBalancer {
  [key: string]: any;
}

export interface InsightsDataServer {
  baseUrl: string;
  [key: string]: any;
}

export interface SharedConfig {
  insightsApp: InsightsApp;
  insightsBalancer: InsightsBalancer;
  insightsDataServer: InsightsDataServer;
}
