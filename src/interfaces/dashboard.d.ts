export interface Dashboard {
  id: string;
  name: string;
  reportIds: (Report['id'][]) | null;
  reports?: (Report[]) | null;
}

interface KeyValue {
  [key: string]: any;
}

export interface Report {
  dataServerQuery?: string;
  dataServerUrl?: string;
  id: string;
  name: string;
  mappedParameters?:KeyValue;
  parameters?: string[];
  parameterMapping?: KeyValue;
  requestMethod: 'post' | 'get';
  requestPayload?: KeyValue;
  responseFields?: (string[]) | null;
  responseMapping?: KeyValue;
  storedProcedure: string;
}
