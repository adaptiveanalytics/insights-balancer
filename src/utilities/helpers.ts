import * as _ from 'lodash';

export function normalizeUrl(url: string[]): string {
  return _.toArray(url).map((u) => {
    if (u.slice(0, 1) === '/') {
      u = u.slice(1);
    }

    if (u.slice(-1) === '/') {
      u = u.slice(0, -1);
    }

    return u;
  }).join('/');
}
