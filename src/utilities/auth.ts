export const getAuthToken = (req, res, next) => {
  const requiredHeaders = ['authorization', 'domain', 'uuid'];
  requiredHeaders.map((h) => {
    if (!req.headers[h]) {
      throw 'Invalid request - missing headers';
    }
  });

  if (
    req.headers.authorization &&
    req.headers.authorization.split(' ')[0] === 'Bearer'
  ) {
    req.authToken = req.headers.authorization.split(' ')[1];
  } else {
    req.authToken = null;
  }

  next();
};
