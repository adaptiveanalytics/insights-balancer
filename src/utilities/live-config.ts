import { PKG } from '../config';
import { camelCase } from 'lodash';
import { SharedConfig } from '../interfaces/system';

export const LiveConfig = {
  app$: null,
  setNamespace: (k) => [PKG.name, camelCase(k)].join('.'),
  setApp: (app) => LiveConfig.app$ = app,
  getAll: () => {
    const keys = Object.keys(LiveConfig.app$.settings).filter(k => k.includes(PKG.name));
    const values = {};
    keys.map(k => {
      const key = k.split('.')[1];
      values[key] = LiveConfig.getVariable(key);
    });
    return values as SharedConfig;
  },
  getVariable: (k) => LiveConfig.app$.get(LiveConfig.setNamespace(k)),
  setVariable: (k, v) => LiveConfig.app$.set(LiveConfig.setNamespace(k), v),
};
