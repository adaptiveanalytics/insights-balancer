import * as admin from 'firebase-admin';
import { ENV } from '../config';
// import devAccount from '../../ih-ego-dev-firebase-adminsdk.json';
import devAccount from '../../xplor-insights-firebase.json';
import prodAccount from '../../ih-ego-firebase-adminsdk.json';

let serviceAccount: admin.ServiceAccount = prodAccount as admin.ServiceAccount;
switch (ENV) {
  case 'dev':
  case 'development':
  case 'local':
  case 'staging':
    serviceAccount = devAccount as admin.ServiceAccount;
    break;
}

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_DB_URL,
});

export default admin;
