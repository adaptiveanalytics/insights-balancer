import fs from 'fs-extra';
import { createLogger, format, transports } from 'winston';
import 'winston-daily-rotate-file';
import moment from 'moment';
import { isObject } from 'lodash';
import { PKG } from '../config';
import { LogConfig } from '../config/log.config';

const { combine, timestamp, label, printf } = format;

const logName = LogConfig.logName || PKG.name;

try {
  fs.ensureDirSync(LogConfig.dir);
} catch (e) {
  console.error(e.stack);
  console.error(e);
}

const devFormat = printf(info => {
  const message = isObject(info.message) ? JSON.stringify(info.message) : info.message;
  const level = info.level.toUpperCase().slice(0, 1);
  const label = info.label.toUpperCase().slice(0, 3);
  const ts = moment(info.timestamp).format('YYYY-MM-DD HH:mm:ss.SSS');

  return `${ts} [${label}] [${level}] ${message}`;
});

const errorTransport = new transports.DailyRotateFile(LogConfig.transports.error);
const combinedTransport = new transports.DailyRotateFile(LogConfig.transports.combined);
const routeTransport = new transports.DailyRotateFile(LogConfig.transports.route);

errorTransport.on('rotate', (oldFilename, newFilename) => {
  console.log(`Upload file ${oldFilename} to DB and create ${newFilename}`);
});

combinedTransport.on('rotate', (oldFilename, newFilename) => {
  console.log(`Upload file ${oldFilename} to DB and create ${newFilename}`);
});

routeTransport.on('rotate', (oldFilename, newFilename) => {
  console.log(`Upload file ${oldFilename} to DB and create ${newFilename}`);
});

const logger = createLogger({
  transports: [errorTransport, combinedTransport],
  format: combine(label({ label: logName }), timestamp(), format.json()),
});

const routeLogger = createLogger({
  transports: [routeTransport],
  format: combine(label({ label: logName }), timestamp(), format.json()),
});

if (process.env.NODE_ENV === 'local') {
  const localConfig = {
    transports: [new transports.Console()],
    format: combine(label({ label: logName }), timestamp(), devFormat)
  };
  routeLogger.configure(localConfig);
  logger.configure(localConfig);
  // logger.add(new winston.transports.Console({format: devFormat}));
  // routeLogger.add(new winston.transports.Console({format: devFormat}));
}

export { logger, routeLogger };
