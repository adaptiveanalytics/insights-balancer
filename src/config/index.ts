import PKG from '../../package.json';

const ENV = process.env.NODE_ENV || 'local';

export { PKG, ENV };
