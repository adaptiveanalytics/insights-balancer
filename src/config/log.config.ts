import path from 'path';

const LOG_DIR = path.join(process.env.BASE_PATH || path.resolve(__dirname, '../..'), 'logs');

export const LogConfig = {
  logName: process.env.LOG_NAME || undefined,
  dir: LOG_DIR,
  level: 'debug',
  levels: [
    'error',
    'warn',
    'info',
    'http',
    'verbose',
    'debug',
    'silly'
  ],
  transports: {
    error: {
      level: 'error',
      frequency: '1h',
      dirname: LOG_DIR,
      filename: 'error-%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d'
    },
    combined: {
      frequency: '1h',
      dirname: LOG_DIR,
      filename: 'combined-%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d'
    },
    route: {
      frequency: '1h',
      dirname: LOG_DIR,
      filename: 'route-%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d'
    }
  }
};
