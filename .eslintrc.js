module.exports = {
  'env': {
    'es6': true,
    'node': true,
    'mocha': true,
    'browser': true,
  },
  'extends': 'eslint:recommended',
  'parserOptions': {
    'sourceType': 'module',
    'ecmaVersion': 2017,
  },
  'rules': {
    'indent': [
      'error',
      2,
      {'SwitchCase': 1}
    ],
    'quotes': [
      'error',
      'single',
      {
        'avoidEscape': true,
        'allowTemplateLiterals': true
      }
    ],
    'no-console': [
      'error',
      {
        'allow': ['log', 'info', 'warn', 'error'],
      },
    ],
    'yoda': [
      'error'
    ],
    'no-unused-vars': [
      'warn',
      {
        'vars': 'local',
      }
    ],
  },
};
