import test from 'unit.js';
import * as helpers from '../../src/utilities/helpers';

describe('Helper Utility Testing', () => {

  const testUrlSegments = ['https://some.url/', '/to/a/path'];

  it('Should normalize a URL', () => {
    const testUrl = helpers.normalizeUrl(testUrlSegments);

    test
      .string(testUrl)
      .startsWith('https')
      .is('https://some.url/to/a/path');

  });

});
