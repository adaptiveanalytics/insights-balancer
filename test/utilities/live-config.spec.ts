import test from 'unit.js';
import express from 'express';
import * as _ from 'lodash';

import PKG from '../../package.json';
import { LiveConfig } from '../../src/utilities/live-config';


describe('LiveConfig Testing', () => {

  const testKey = 'foo';
  const testValue = 'bar';
  const testObj = { [testKey]: testValue };
  const updatedValue = 'baz';
  const app = express();

  before(() => {
    LiveConfig.setApp(app);
  });

  it('Should set the namespace', () => {
    test
      .string(LiveConfig.setNamespace(testKey))
      .is([PKG.name, _.camelCase(testKey)].join('.'));
  })

  it('Should store a key/value', () => {
    test
      .function(LiveConfig.setVariable(testKey, testValue))
      .string(app.get(LiveConfig.setNamespace(testKey)))
      .is(testValue);
  });

  it('Should retrieve a key/value', () => {
    test
      .string(LiveConfig.getVariable(testKey))
      .is(testValue);
  });

  it('Should retrieve all', () => {
    test
      .object(LiveConfig.getAll())
      .is(testObj);
  });

  it('Should update a key/value', () => {
    test
      .function(LiveConfig.setVariable(testKey, updatedValue))
      .string(LiveConfig.getVariable(testKey))
      .is(updatedValue);
  });

});
