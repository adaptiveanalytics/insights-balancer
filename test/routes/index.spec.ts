import test from 'unit.js';
import { createApp, destroyApp } from './scaffold';

const route = '/';
const baseUrl = `http://localhost:3000`;

describe(`Route ${route}`, () => {

  beforeEach((done) => {
    createApp(done);
  });

  afterEach((done) => {
    destroyApp(done);
  });

  it('/health-check : should respond healthy', (done) => {
    test.httpAgent(baseUrl)
      .get('/health-check')
      .expect('Content-Type', /json/)
      .expect((res) => {
        test.assert.equal(res.body.healthy, true);
      })
      .expect(200, done);
  });

  it('/?debug=true : should enable debug in local', (done) => {
    test.httpAgent(baseUrl)
      .get('/health-check?debug=true')
      .expect('Content-Type', /json/)
      .expect((res) => {
        test.assert.equal(res.body.user.debugMode, true);
      })
      .expect(200, done);
  });

});
