import test from 'unit.js';
import { createApp, destroyApp } from '../scaffold';

const route = '/system/logs';
const baseUrl = `http://localhost:3000${route}`;

describe(`Route ${route}`, () => {

  let firstFile;

  beforeEach((done) => {
    createApp(done);
  });

  afterEach((done) => {
    destroyApp(done);
  });

  it('/health-check : should respond healthy', (done) => {
    test.httpAgent(baseUrl)
      .get('/health-check')
      .expect('Content-Type', /json/)
      .expect((res) => {
        test.assert.equal(res.body.healthy, true);
      })
      .expect(200, done);
  });

  it('/list : should return list of all log files on server', (done) => {
    test.httpAgent(baseUrl)
      .get('/list')
      .expect('Content-Type', /json/)
      .expect(({ body }) => {
        firstFile = body[0];
        test.should(firstFile.includes('.log')).equal(true);
        // console.log(JSON.stringify(body));
      })
      .expect(200, done);
  });

  it('/list/:level : should return list of all log files on server', (done) => {
    test.httpAgent(baseUrl)
      .get('/list/combined')
      .expect('Content-Type', /json/)
      .expect(({ body }) => {
        test.should(body[0].includes('.log')).equal(true);
        // console.log(JSON.stringify(body));
      })
      .expect(200, done);
  });

  it('/details/:filename : should return log file contents', (done) => {
    test.httpAgent(baseUrl)
      .get(`/details/${firstFile}`)
      .expect(({ body }) => {
        test.should.exist(body);
        // console.log(JSON.stringify(body));
      })
      .expect(200, done);
  });

});
