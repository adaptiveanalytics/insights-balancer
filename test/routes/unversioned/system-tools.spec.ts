import test from 'unit.js';
import { createApp, destroyApp } from '../scaffold';

const route = '/system/tools';
const baseUrl = `http://localhost:3000${route}`;

describe(`Route ${route}`, () => {

  beforeEach((done) => {
    createApp(done);
  });

  afterEach((done) => {
    destroyApp(done);
  });

  it('/health-check : should respond healthy', (done) => {
    test.httpAgent(baseUrl)
      .get('/health-check')
      .expect('Content-Type', /json/)
      .expect((res) => {
        test.assert.equal(res.body.healthy, true);
      })
      .expect(200, done);
  });

});
