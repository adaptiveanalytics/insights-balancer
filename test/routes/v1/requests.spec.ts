import test from 'unit.js';
import { createApp, destroyApp, postHeaders } from '../scaffold';

const route = '/v1/requests';
const baseUrl = `http://localhost:3000${route}`;
const validDashboardRequest = {
  'id': '123',
  'parameters': {
    'date_start': '2022-05-01',
    'date_end': '2022-05-31',
    'locationIds': [
      '41363',
      '41364',
      '41365',
      '41366'
    ]
  }
};
const validReportRequest = {
  'id': 'Yi9bFXd1DWVef1bnBa2K',
  'parameters': {
    'date_start': '2022-05-01',
    'date_end': '2022-05-31',
    'locationIds': [
      '41363',
      '41364',
      '41365',
      '41366'
    ]
  }
};
const invalidDashboardRequest = {
  'id': '123',
  'parameters': {}
};
const invalidReportRequest = {
  'id': 'Yi9bFXd1DWVef1bnBa2K',
  'parameters': {}
};

describe(`Route ${route}`, () => {

  beforeEach((done) => {
    createApp(done);
  });

  afterEach((done) => {
    destroyApp(done);
  });

  it('/health-check : should respond healthy', (done) => {
    test.httpAgent(baseUrl)
      .get('/health-check')
      .expect('Content-Type', /json/)
      .expect((res) => {
        test.assert.equal(res.body.healthy, true);
      })
      .expect(200, done);
  });

  it('/ : should reject requests without headers', (done) => {
    test.httpAgent(baseUrl)
      .get('/')
      .expect('Content-Type', /json/)
      .expect((res) => {
        test.assert.equal(res.body.err, 'Invalid request - missing headers');
      })
      .expect(500, done);
  });

  it('/dashboard : should return a dashboard request', (done) => {
    test.httpAgent(baseUrl)
      .post('/dashboard')
      .set(postHeaders)
      .send(validDashboardRequest)
      .expect('Content-Type', /json/)
      .expect(({ body }) => {
        body.should.have.property('name');
      })
      .expect(200, done);
  });

  it('/dashboard : should reject an invalid dashboard request', (done) => {
    test.httpAgent(baseUrl)
      .post('/dashboard')
      .set(postHeaders)
      .send(invalidDashboardRequest)
      .expect('Content-Type', /json/)
      .expect(({ body }) => {
        body.should.have.property('name', 'ValidationError');
      })
      .expect(400, done);
  });

  it('/report : should return a report request', (done) => {
    test.httpAgent(baseUrl)
      .post('/report')
      .set(postHeaders)
      .send(validReportRequest)
      .expect('Content-Type', /json/)
      .expect(({ body }) => {
        body.should.have.property('requestMethod');
      })
      .expect(200, done);
  });

  it('/report : should reject an invalid report request', (done) => {
    test.httpAgent(baseUrl)
      .post('/dashboard')
      .set(postHeaders)
      .send(invalidReportRequest)
      .expect('Content-Type', /json/)
      .expect(({ body }) => {
        body.should.have.property('name', 'ValidationError');
      })
      .expect(400, done);
  });
});
