import test from 'unit.js';
import { createApp, destroyApp } from '../scaffold';

const route = '/v1/hooks';
const baseUrl = `http://localhost:3000${route}`;
const variableKey = 'insights_app';

describe(`Route ${route}`, () => {

  beforeEach((done) => {
    createApp(done);
  });

  afterEach((done) => {
    destroyApp(done);
  });

  it('/health-check : should respond healthy', (done) => {
    test.httpAgent(baseUrl)
      .get('/health-check')
      .expect('Content-Type', /json/)
      .expect((res) => {
        test.assert.equal(res.body.healthy, true);
      })
      .expect(200, done);
  });

  it('/machine-variable/all : should return all machine variables', (done) => {
    test.httpAgent(baseUrl)
      .get('/machine-variable/all')
      .expect('Content-Type', /json/)
      .expect((res) => {
        res.body.should.have.property('insightsApp');
        res.body.should.have.property('insightsBalancer');
        res.body.should.have.property('insightsDataServer');
      })
      .expect(200, done);
  });

  it('/machine-variable/:key : should return a specific machine variable', (done) => {
    test.httpAgent(baseUrl)
      .get(`/machine-variable/${variableKey}`)
      .expect('Content-Type', /json/)
      .expect((res) => {
        res.body.should.have.property('adminPermissions');
        res.body.should.have.property('userPermissions');
      })
      .expect(200, done);
  });

});
