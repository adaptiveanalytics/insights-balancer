import test from 'unit.js';
import { authHeaders, createApp, destroyApp } from '../scaffold';

const route = '/v1/mt';
const baseUrl = `http://localhost:3000${route}`;
const domain = 'adaptiveanalytics.sandbox';
const userId = '34736';
const email = 'adaptive_analytics-t3oa-service@marianatek.com';

describe(`Route ${route}`, () => {

  beforeEach((done) => {
    createApp(done);
  });

  afterEach((done) => {
    destroyApp(done);
  });

  it('/health-check : should respond healthy', (done) => {
    test.httpAgent(baseUrl)
      .get('/health-check')
      .expect('Content-Type', /json/)
      .expect((res) => {
        test.assert.equal(res.body.healthy, true);
      })
      .expect(200, done);
  });

  it('/ : should reject requests without headers', (done) => {
    test.httpAgent(baseUrl)
      .get('/?debug=true')
      .expect((res) => {
        test.assert.equal(res.body.err, 'Invalid request - missing headers');
      })
      .expect(500, done);
  });

  it('/locations : should return the user locations', (done) => {
    test.httpAgent(baseUrl)
      .get('/locations')
      .set(authHeaders)
      .expect(({ body }) => {
        test.should(body).have.lengthOf(6);
      })
      .expect(200, done);
  }).timeout(5000);

  it('/self : should return the user object', (done) => {
    test.httpAgent(baseUrl)
      .get('/self')
      .set(authHeaders)
      .expect(({ body }) => {
        body.should.have.property('userId', userId);
      })
      .expect(200, done);
  });

  it('/user/object/:domain/:email : should return the user object', (done) => {
    test.httpAgent(baseUrl)
      .get(`/user/object/${domain}/${email}`)
      .set(authHeaders)
      .expect(({ body }) => {
        body.self.should.have.property('userId', userId);
      })
      .expect(200, done);
  }).timeout(10000);

  it('/user/locations/:domain/:userId : should return the user locations', (done) => {
    test.httpAgent(baseUrl)
      .get(`/user/locations/${domain}/${userId}`)
      .set(authHeaders)
      .expect(({ body }) => {
        test.should(body).have.lengthOf(6);
      })
      .expect(200, done);
  }).timeout(5000);

  it('/domain/partners/:domain : should return the domain partners', (done) => {
    test.httpAgent(baseUrl)
      .get(`/domain/partners/${domain}`)
      .set(authHeaders)
      .expect(({ body }) => {
        // console.log(JSON.stringify(body));
        body.should.have.property('meta');
      })
      .expect(200, done);
  }).timeout(5000);

  it('/domain/locations/:domain : should return the domain locations', (done) => {
    test.httpAgent(baseUrl)
      .get(`/domain/locations/${domain}`)
      .set(authHeaders)
      .expect(({ body }) => {
        // console.log(JSON.stringify(body));
        body.should.have.property('meta');
      })
      .expect(200, done);
  }).timeout(5000);

  it('/domain/employees/fb/:domain : should return the domain locations', (done) => {
    test.httpAgent(baseUrl)
      .get(`/domain/employees/fb/${domain}`)
      .set(authHeaders)
      .expect(({ body }) => {
        // console.log(JSON.stringify(body));
        body.should.have.property('meta');
      })
      .expect(200, done);
  }).timeout(5000);

  it('/domain/employees/:domain : should return the domain locations', (done) => {
    test.httpAgent(baseUrl)
      .get(`/domain/employees/${domain}`)
      .set(authHeaders)
      .expect(({ body }) => {
        // console.log(JSON.stringify(body));
        body.should.have.property('meta');
      })
      .expect(200, done);
  }).timeout(5000);

});
