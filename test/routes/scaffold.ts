import express from 'express';
import Routes from '../../src/routes';
import { BaseCtrl } from '../../src/controllers/base.ctrl';
import { LiveConfig } from '../../src/utilities/live-config';
import bodyParser from 'body-parser';

const app = express();
let server;

const adminToken = process.env.MT_ADMIN_TOKEN = 'Bo7SGvCzoL4imKaLXeOzpFL36uAEBO';

export const authHeaders = {
  uuid: '70785c6e-b9b2-49be-b70c-25f71a5f318a',
  Authorization: `Bearer ${adminToken}`,
  domain: 'adaptiveanalytics.sandbox'
};
export const postHeaders = { Accept: 'application/json', localbypass: true };

export function createApp(done) {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  const baseCtrl = new BaseCtrl('unit-test');
  LiveConfig.setApp(app);
  baseCtrl.getAllConfigs()
    .then(() => {
      Routes(app);
      server = app.listen(3000);
      done();
    })
    .catch(e => console.error({ e, stack: e.stack }));

}

export function destroyApp(done) {
  server.close();
  done();
}
