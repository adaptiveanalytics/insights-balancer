# Insights by Xplor Load Balancer

Insights by Xplor Express JS API in Typescript.

## Requirements

* [Node v16](https://nodejs.org)
* [Docker Desktop](https://www.docker.com/products/docker-desktop)

## Installation

```shell
$ git clone git@bitbucket.org:adaptiveanalytics/insights-balancer.git
$ cd ./insights-balancer
$ npm ci
$ cp ./config/.env.dev ./.env   # Update

# Docker start up - first time
$ docker-compose up --build     # CTRL+Z to keep running

# Docker subsequent start up
$ docker-compose up             # CTRL+Z to keep running

# Docker stop
$ docker-compose down
```


### Authors

* Bill Brady <bill.brady@xplortechnologies.com>
* Alec Juknievich <alec.juknievich@xplortechnologies.com>
* James Heitmiller <james.heitmiller@xplortechnologies.com>
* Hunter Jones <hunter.jones@xplortechnologies.com>
