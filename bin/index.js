'use strict';

require('dotenv').config();

const path = require('path');

process.env.BASE_PATH = process.env.BASE_PATH || path.resolve(__dirname, '..');

const dir = process.env.NODE_ENV !== 'local' ? 'dist/src' : 'src';

require(path.resolve(dir));
