module.exports = {
  apps: [
    {
      name: 'insights-balancer',
      script: 'bin/index.js',
      exec_mode: 'cluster',
      instances: 'max',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'local',
        NODE_PORT: 3000,
      },
      env_production: {
        NODE_ENV: 'production',
        NODE_PORT: 8081,
        BASE_PATH: '/var/app/current',
      },
      env_staging: {
        NODE_ENV: 'staging',
        NODE_PORT: 8081,
        BASE_PATH: '/var/app/current',
      }
    }
  ]
}
